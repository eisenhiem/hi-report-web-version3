import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  // token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    // this.token = sessionStorage.getItem('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + this.token
      })
    };
  }

  async getSummaryOp() {        
    const _url = `${this.apiUrl}/dashboard/summary-op`;
    console.log(_url);
    return this.httpClient.get(_url, this.httpOptions)
    .toPromise();
  }

  async getSummaryIp() {        
    const _url = `${this.apiUrl}/dashboard/summary-ip`;
    console.log(_url);
    return this.httpClient.get(_url, this.httpOptions)
    .toPromise();
  }

  async getTopOpDiag() {        
    const _url = `${this.apiUrl}/dashboard/top-op-diag`;
    console.log(_url);
    return this.httpClient.post(_url, this.httpOptions)
    .toPromise();
  }

  async getTopIpDiag() {        
    const _url = `${this.apiUrl}/dashboard/top-ip-diag`;
    console.log(_url);
    return this.httpClient.post(_url, this.httpOptions)
    .toPromise();
  }

  async getSummaryClinic() {        
    const _url = `${this.apiUrl}/dashboard/summary-clinic`;
    console.log(_url);
    return this.httpClient.post(_url, this.httpOptions)
    .toPromise();
  }

  async getSummaryInsurance() {        
    const _url = `${this.apiUrl}/dashboard/summary-insurance`;
    console.log(_url);
    return this.httpClient.post(_url, this.httpOptions)
    .toPromise();
  }

}
