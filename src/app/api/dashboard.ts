export interface SummaryOp {
    yearbudget: number,
    visit: number,
    people: number,
    admit: number,
    peopleAdmit: number,
    refer_op: number,
    death: number
}

export interface SummaryIp {
    yearbudget: number,
    people: number,
    admit: number,
    refer: number,
    death: number,
    losd: any,
    days: number,
    bed_rate: any,
}

export interface TopDiag {
    cgroup: number,
    e_name: string,
    icd10: string,
    t_name: string,
    total: number
}

export interface ChartDataSet {
    label: string,
    data: any,
    fill: boolean,
    backgroundColor: string,
    borderColor: string,
    tension: number
}

export interface SummaryClinic {
    cln: string,
    namecln: string,
    total: number,
    admit: number,
    refer: number,
    death: number
}

export interface SummaryInsurane {
    income_id: number,
    incomeName: string,
    total: number,
    admit: number,
    refer: number,
    death: number,
    amount: number;
    paid: number;
    rate: number;
    width: number;
}
