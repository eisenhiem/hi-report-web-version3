import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Product } from '../../api/product';
import { ProductService } from '../../service/productservice';
import { Subscription } from 'rxjs';
import { ConfigService } from '../../service/app.config.service';
import { DashboardService } from '../../api-services/dashboard.service';
import { AppConfig } from '../../api/appconfig';
import { SummaryIp, SummaryOp, TopDiag, SummaryClinic, ChartDataSet, SummaryInsurane } from '../../api/dashboard';

@Component({
    templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {

    items: MenuItem[];

    products: Product[];

    chartLabels: any[];

    chartData: any;

    chartData1: any;

    chartIp: any;

    chartBedRate: any;

    chartClinic: any;

    chartOptions: any;

    subscription: Subscription;

    config: AppConfig;

    constructor(
        private productService: ProductService,
        public configService: ConfigService,
        private dashboardService: DashboardService
    ) { }

    ngOnInit() {
        this.config = this.configService.config;
        this.subscription = this.configService.configUpdate$.subscribe(config => {
            this.config = config;
            this.updateChartOptions();
        });
        this.productService.getProductsSmall().then(data => this.products = data);

        this.getSummaryOp(); // get data OP summary
        this.getSummaryIp(); // get data IP summary
        this.getTopOpDiag(); // get data Top diagnosis Opd
        this.getTopIpDiag(); // get data Top diagnosis Ipd
        this.getSummaryClinic(); // get data Summary Clininc service current date
        this.getSummaryInsurance(); // get data Summary Insurance service current date

        this.items = [
            { label: 'Add New', icon: 'pi pi-fw pi-plus' },
            { label: 'Remove', icon: 'pi pi-fw pi-minus' }
        ];

        this.chartData = {
            labels: ['2018', '2019', '2020', '2021', '2022'],
            //labels: this.chartLabels,
            datasets: [
                {
                    label: 'First Dataset',
                    data: [45, 59, 80, 81, 56],
                    //data: this.chartLabels,
                    fill: false,
                    backgroundColor: '#2f4860',
                    borderColor: '#2f4860',
                    tension: .4
                },
                {
                    label: 'Second Dataset',
                    data: [28, 48, 40, 19, 86],
                    fill: false,
                    backgroundColor: '#00bb7e',
                    borderColor: '#00bb7e',
                    tension: .4
                }
            ]
        };

    }

    updateChartOptions() {
        if (this.config.dark)
            this.applyDarkTheme();
        else
            this.applyLightTheme();

    }

    applyDarkTheme() {
        this.chartOptions = {
            plugins: {
                legend: {
                    labels: {
                        color: '#ebedef'
                    }
                }
            },
            scales: {
                x: {
                    ticks: {
                        color: '#ebedef'
                    },
                    grid: {
                        color: 'rgba(160, 167, 181, .3)',
                    }
                },
                y: {
                    ticks: {
                        color: '#ebedef'
                    },
                    grid: {
                        color: 'rgba(160, 167, 181, .3)',
                    }
                },
            }
        };
    }

    applyLightTheme() {
        this.chartOptions = {
            plugins: {
                legend: {
                    labels: {
                        color: '#495057'
                    }
                }
            },
            scales: {
                x: {
                    ticks: {
                        color: '#495057'
                    },
                    grid: {
                        color: '#ebedef',
                    }
                },
                y: {
                    ticks: {
                        color: '#495057'
                    },
                    grid: {
                        color: '#ebedef',
                    }
                },
            }
        };
    }

    blockedDocument: boolean = false;
    loading: boolean = false;

    yearbudget: any; // รับค่าปีงบประมาณ ค.ศ.
    getDate: any; // รับค่าวันที่
    byMonth: any; // byMonth == 1 => สรุปแบบรายเดือน
    byYearbudget: any; // byYearbudget == 1 => สรุปแบบรายปีงบประมาณ
    byYear: any; // byYear == 1 => สรุปแบบรายปี พ.ศ.

    dataSummaryOp: any;
    summaryOp: SummaryOp[];
    visit: number[];
    people: number[];
    data3: number;
    data4: number;
    data5: number;

    async getSummaryOp() {
        this.blockedDocument = true;
        this.loading = true;

        try {
            let rs: any = await this.dashboardService.getSummaryOp();
            // console.log(rs);
            this.dataSummaryOp = rs;
            this.summaryOp = rs.results.data;
            console.log('summary op data', this.summaryOp);
            this.chartLabels = [];
            this.visit = [];
            this.people = [];
            for (var i of this.summaryOp) {
                let label: string = i.yearbudget.toString();
                let visit: number = i.visit;
                let people: number = i.people;
                //let data :number = i.visit;
                this.chartLabels.push(label);
                this.visit.push(visit);
                this.people.push(people);
            }
            console.log(`visit`, this.visit); // prints values: 10, 20, 30, 40
            this.chartData1 = {
                labels: this.chartLabels,
                //labels: this.chartLabels,
                datasets: [
                    {
                        label: 'การให้บริการ(ครั้ง)',
                        data: this.visit,
                        //data: this.chartLabels,
                        fill: false,
                        backgroundColor: '#019267',
                        borderColor: '#019267',
                        tension: .4
                    },
                    {
                        label: 'การให้บริการ(รายคน)',
                        data: this.people,
                        //data: this.chartLabels,
                        fill: false,
                        backgroundColor: '#39A9CB',
                        borderColor: '#39A9CB',
                        tension: .4
                    },
                ]
            };
            console.log('data chart', this.chartData1);

        } catch (error) {
            console.log(error);
        }

        this.loading = false;
        this.blockedDocument = false;
    }

    dataSummaryIp: any;
    summaryIp: SummaryIp[];
    admit: number[];
    losd: number[];
    bed_rate: number[];

    async getSummaryIp() {
        this.blockedDocument = true;
        this.loading = true;

        try {
            let rs: any = await this.dashboardService.getSummaryIp();
            // console.log(rs);
            this.dataSummaryIp = rs;
            this.summaryIp = rs.results.data;
            console.log('summary ip data', this.summaryIp);
            this.chartLabels = [];
            this.admit = [];
            this.losd = [];
            this.bed_rate = [];
            for (var i of this.summaryIp) {
                let label: string = i.yearbudget.toString();
                let admit: number = i.admit;
                let losd: number = i.losd;
                let bed_rate: number = i.bed_rate;
                //let data :number = i.visit;
                this.chartLabels.push(label);
                this.admit.push(admit);
                this.losd.push(losd);
                this.bed_rate.push(bed_rate);
            }
            console.log(`visit`, this.admit); // prints values: 10, 20, 30, 40
            this.chartIp = {
                labels: this.chartLabels,
                //labels: this.chartLabels,
                datasets: [
                    {
                        label: 'ผู้ป่วยใน(ราย)',
                        data: this.admit,
                        //data: this.chartLabels,
                        fill: false,
                        backgroundColor: '#FFD365',
                        borderColor: '#FFD365',
                        tension: .4
                    },
                    {
                        label: 'วันนอน(วัน)',
                        data: this.losd,
                        //data: this.chartLabels,
                        fill: false,
                        backgroundColor: '#A04860',
                        borderColor: '#A04860',
                        tension: .4
                    },
                ]
            };
            console.log('data chart', this.chartIp);
            this.chartBedRate = {
                labels: this.chartLabels,
                //labels: this.chartLabels,
                datasets: [
                    {
                        label: 'อัตราการครองเตียง(%)',
                        data: this.bed_rate,
                        //data: this.chartLabels,
                        fill: false,
                        backgroundColor: '#E99497',
                        borderColor: '#E99497',
                        tension: .4
                    },
                ]
            };
            console.log('data chart', this.chartBedRate);

        } catch (error) {
            console.log(error);
        }

        this.loading = false;
        this.blockedDocument = false;
    }

    dataTopOpDiag: any;
    topOpDiag: TopDiag[];

    async getTopOpDiag() {
        this.blockedDocument = true;
        this.loading = true;

        try {
            let rs: any = await this.dashboardService.getTopOpDiag();
            // console.log(rs);
            this.dataTopOpDiag = rs;
            this.topOpDiag = rs.results.data;
            console.log('diag op data', this.topOpDiag);


        } catch (error) {
            console.log(error);
        }

        this.loading = false;
        this.blockedDocument = false;
    }

    dataTopIpDiag: any;
    topIpDiag: TopDiag[];

    async getTopIpDiag() {
        this.blockedDocument = true;
        this.loading = true;

        try {
            let rs: any = await this.dashboardService.getTopIpDiag();
            // console.log(`diag ip result`,rs);
            this.dataTopIpDiag = rs;
            this.topIpDiag = rs.results.data;
            console.log('diag ip data', this.topIpDiag);


        } catch (error) {
            console.log(error);
        }

        this.loading = false;
        this.blockedDocument = false;
    }

    dataSummaryClinic: any;
    summaryClinic: SummaryClinic[];
    total: number[];
    day_admit: number[];
    refer: number[];
    death: number[];

    async getSummaryClinic() {
        this.blockedDocument = true;
        this.loading = true;

        try {
            let rs: any = await this.dashboardService.getSummaryClinic();
            // console.log(rs);
            this.dataSummaryClinic = rs;
            this.summaryClinic = rs.results.data;
            console.log('summary clinic data', this.summaryClinic);
            this.chartLabels = [];
            this.total = [];
            this.day_admit = [];
            this.refer = [];
            this.death = [];

            for (var i of this.summaryClinic) {
                let label: string = i.namecln;
                let total: number = i.total;
                let admit: number = i.admit;
                let refer: number = i.refer;
                let death: number = i.death;

                //let data :number = i.visit;
                this.chartLabels.push(label);
                this.total.push(total);
                this.day_admit.push(admit);
                this.refer.push(refer);
                this.death.push(death);
            }
            console.log(`clinic`, this.total); // prints values: 10, 20, 30, 40
            this.chartClinic = {
                labels: this.chartLabels,
                //labels: this.chartLabels,
                datasets: [
                    {
                        label: 'การให้บริการ(ครั้ง)',
                        data: this.total,
                        //data: this.chartLabels,
                        fill: false,
                        backgroundColor: '#019267',
                        borderColor: '#019267',
                        tension: .4
                    },
                    {
                        label: 'Admit(ราย)',
                        data: this.day_admit,
                        //data: this.chartLabels,
                        fill: false,
                        backgroundColor: '#FFD365',
                        borderColor: '#FFD365',
                        tension: .4
                    },
                    {
                        label: 'Refer(ราย)',
                        data: this.refer,
                        //data: this.chartLabels,
                        fill: false,
                        backgroundColor: '#FF6363',
                        borderColor: '#FF6363',
                        tension: .4
                    },
                    {
                        label: 'Death(ราย)',
                        data: this.death,
                        //data: this.chartLabels,
                        fill: false,
                        backgroundColor: '#595260',
                        borderColor: '#595260',
                        tension: .4
                    },
                ]
            };
            console.log('data chart', this.chartData1);

        } catch (error) {
            console.log(error);
        }

        this.loading = false;
        this.blockedDocument = false;
    }

    dataSummaryInsurance: any;
    summaryInsurance: SummaryInsurane[];
    ins_total: number;

    async getSummaryInsurance() {
        this.blockedDocument = true;
        this.loading = true;
        this.ins_total = 0;

        try {
            let rs: any = await this.dashboardService.getSummaryInsurance();
            // console.log(`diag ip result`,rs);
            this.dataSummaryInsurance = rs;
            this.summaryInsurance = rs.results.data;
            console.log('insurance ', this.summaryInsurance);
            for (var i of this.summaryInsurance) {
                this.ins_total = this.ins_total + i.amount*1;
            }
            console.log('ins-total',this.ins_total);
            for (var i of this.summaryInsurance) {
                i.rate =  i.amount*100/this.ins_total;
                i.width = Math.round(i.rate);
            }
            console.log('after insurance ', this.summaryInsurance);


        } catch (error) {
            console.log(error);
        }

        this.loading = false;
        this.blockedDocument = false;
    }


}
